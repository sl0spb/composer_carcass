up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear clear docker-pull docker-build docker-up app-init
require: composer-require

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

clear:
	docker run --rm -v ${PWD}/application:/app --workdir=/app alpine rm -f .ready

app-init: composer-install wait-db app-ready

wait-db:
	until docker-compose exec -T postgres pg_isready --timeout=0 --dbname=app_db ; do sleep 1 ; done

app-ready:
	docker run --rm -v ${PWD}/application:/app --workdir=/app alpine touch .ready

composer-install:
	docker-compose run --rm php-cli composer install

composer-require:
	docker-compose run --rm php-cli composer require $(app)

